# N64-Game-ID / Per Game Settings for N64Digital

N64Digital allows per game settings when a game id is sent over the controller bus (joybus).

## Game ID

*Byte offsets refer to the position in native rom format*

| Byte | Description |
| ----- | ------- |
| `0x00`<br/>`0x01`<br/>`0x02`<br/>`0x03` | CRC HI / CRC1<br/>(bytes `[0x10-0x13]` of the ROM) |
| `0x04`<br/>`0x05`<br/>`0x06`<br/>`0x07` | CRC LO / CRC2<br/>(bytes `[0x14-0x17]` of the ROM) |
| `0x08` | Media format ROM type<br/>(byte `0x3B` of the ROM) |
| `0x09` | Country Code<br/>(byte `0x3E` of the ROM) |

Example:

- `0xB3 0x0E 0xD9 0x78 0x30 0x03 0xC9 0xF9 0x43 0x45` for F-ZERO X

#### Special IDs

- `0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00` clears the Game ID on N64Digital.

## Controller packet

The Game ID is transmitted by a custom controller packet on port 0 using the command `0x1D`

| Byte   | Value   | Description    |
| ------ | ------- | -------------- |
| `0x00` | `0x1D`  | Joybus command |
| `0x01`<br/>`0x02`<br/>`0x03`<br/>`0x04`<br/>`0x05`<br/>`0x06`<br/>`0x07`<br/>`0x08`<br/>`0x09`<br/>`0x10` | Game ID | Game ID as described above |
| `STOP` | -- | Console Stop Bit |

*N64Digital does not respond to the packet, as it's operation as a bus sniffer only.*

## How to integrate on N64

- Using [libdragon](https://github.com/DragonMinded/libdragon):

```c
void send_game_id(uint8_t* crc_hi, uint8_t* crc_lo, uint8_t media_format, uint8_t country_id)
{
    #define N64_CONTROLLER_PORT 0
    #define GAME_ID_COMMAND 0x1D
    
    uint8_t out[10];
    uint8_t dummy[1];

    memcpy(out, crc_hi, 4);
    memcpy(&out[4], crc_lo, 4);

    out[8] = media_format;
    out[9] = country_id;

    execute_raw_command(N64_CONTROLLER_PORT, GAME_ID_COMMAND, sizeof(out), sizeof(dummy), out, dummy);
}

int main(void)
{
    send_game_id(
        (uint8_t[]){ 0xB3, 0x0E, 0xD9, 0x78, }, // CRC_HI
        (uint8_t[]){ 0x30, 0x03, 0xC9, 0xF9, }, // CRC_LO
        0x43,                                   // ROM type
        0x45                                    // Country code
    );
}

```

- There's also a standalone c implementation to integrate into projects not based on libdragon (tested with mips gcc): [Game ID standalone c implementaion](https://gitlab.com/pixelfx-public/n64-game-id/-/tree/master/src)

## Game DB

N64Digital can also display the name of the game. To map a Game ID to a game, an sqlite3 database is created from Project64's [Config/Project64.rdb](https://github.com/project64/project64/blob/develop/Config/Project64.rdb)

The latest Game DB will be available here: [firmware.pixelfx.co/n64digital/game.db](https://firmware.pixelfx.co/n64digital/game.db)