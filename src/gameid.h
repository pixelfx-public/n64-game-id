#ifndef __GAMEID_H
#define __GAMEID_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

void send_game_id(uint8_t* crc_hi, uint8_t* crc_lo, uint8_t media_format, uint8_t country_id);

#ifdef __cplusplus
}
#endif

#endif
